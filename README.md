Tony's dotfiles
====
When I move to a new machine, I install git, clone this repo and
run the install script. This sets up various files in my home directory.
The install script changes nothing outside of my home directory
and is idempotent.

Requirement
----
The basic functionality requires POSIX.1 or later.

How to use
----
````
./install
````
It is recommended to both run the install script and start a login shell (using
e.g. `sh -l` or `bash -l`) after any changes.

Implementation note
----
Works for any user name, any user ID, and for non-default home directory
locations (i.e. not in `/home`).

FAQ
----
#### Q
Why are some of the scripts in the `bin` directory one line long, when others
are one line of real code with a screenful of error checking beforehand?
#### A
The commands called in the one-line scripts do a good enough job at handling
errors on their own. The commands called in the longer scripts do not, so they
are supplemented.

#### Q
Why is the `-h` option documented in some of the scripts but not in others?
#### A
The `-h` option is so common that is does not deserve a whole block of text
to describe it when it is the only option. If there are other options, it is
acceptable to add one additional line so that all options are listed.

#### Q
Why not use symlinks instead of copying the dotfiles into place?
#### A
The installed scripts must be in a working state at all times. Otherwise,
if configuration of the login shell fails, the user will be unable to login.
If symlinks are used, the scripts in the repository are the installed scripts.
This hampers development. A secondary reason is that symlinks
[do not work consistently](https://www.joshkel.com/2018/01/18/symlinks-in-windows/)
on all platforms that nominally have them.

#### Q
How do I call the scripts that take file names as command arguments across many
directories?
#### A
If you need to filter: `find -name '* *' -mtime -10 -exec despace '{}' \;`.
If you do not need to filter: `despace ./*/*/*`.
