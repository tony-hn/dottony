. "$HOME/.config/shell/appvar"
. "$HOME/.config/shell/dirvar"
. "$HOME/.config/shell/histvar"

# If running bash and bashrc exists, use bashrc. Useful for ssh.
[ -n "$BASH_VERSION" ] && [ -f "$HOME/.bashrc" ] && . "$HOME/.bashrc"
