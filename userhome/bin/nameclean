#! /bin/sh
set -e
. z_usagf

usagstr=`cat << EOF
Usage: nameclean [dir]

Remove any single quote, tab, newline or backtick characters from the names
of files recursively from dir. Requires GNU sed.
If removing these characters causes a name clash, silently overwrite.
If dir is not specified, run from the current directory.
EOF`

# Check command line options and arguments.

if [ "$#" -gt 1 ]; then
  usagf 1;
elif [ "$1" = "--help" ]; then
  usagf 0
fi

while getopts "h" option; do
  case "$option" in
    h) usagf 0 ;;
    *) exit 1 ;;
  esac
done

if [ -z "$1" ]; then xdir="."; else xdir="$1"; fi

# Do the work.

xstar_t=`printf "*\t*"`
xstar_n=`printf "*\n*"`
xstar_b=`printf '*\`*'`

# Single quote.
find "$xdir" -name '*'\''*' -print0 | sed -z "p;s/'//g;" | xargs -r0 -n2 mv

# Tab.
find "$xdir" -name "$xstar_t" -print0 | sed -z 'p;s/\t//g' | xargs -r0 -n2 mv

# Newline.
find "$xdir" -name "$xstar_n" -print0 | sed -z 'p;s/\n//g' | xargs -r0 -n2 mv

# Backtick.
find "$xdir" -name "$xstar_b" -print0 | sed -z 'p;s/`//g' | xargs -r0 -n2 mv
