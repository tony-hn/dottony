#! /bin/sh
. z_usagf

usagstr=`cat << EOF
Usage: tsync [options] <src> <dst>

Incrementally copy data from src to dst.

We usually put a trailing slash on dst only.
Example: tsync -is /srv /media/user/SRV/

OPTIONS
  -h    Show this help message.
  -i    Output a change-summary for all updates.
  -n    Perform a trial run with no changes made.
  -s    Stop after 10 minutes.
EOF`

# Check command line options and arguments.

if [ "$1" = "--help" ]; then usagf 0; fi

opti="-h"
optn="-h"
opts="600"

while getopts "hins" option; do
  case "$option" in
    h) usagf 0 ;;
    i) opti="-hi" ;;
    n) optn="-n" ;;
    s) opts="10" ;;
    *) exit 11 ;;
  esac
done
shift $((OPTIND - 1))

if [ "$#" -ne 2 ]; then
  echo "$0: there must be two non-option arguments" >&2
  exit 13
fi

rsync "$opti" "$optn" -a -tAUUX \
  --delete \
  --open-noatime \
  --exclude='lost+found/' \
  --modify-window=1 \
  --no-D \
  --stop-after="$opts" \
  "$1" "$2"
