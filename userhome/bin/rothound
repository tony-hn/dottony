#! /bin/sh
. z_check
. z_usagf

usagstr=`cat << EOF
Usage: rothound <subcommand> <abs> <rel>

Monitor files for undesired changes such as bit rot, using hashes.
Valid subcommands are check, comm, count, create, update.
Argument abs should be an absolute path specifying a directory.
Argument rel should be a path relative to abs, also specifying a directory.
Output files are named using the rightmost-directory in abs, plus rel.
To run near the root of the filesystem, e.g. at /etc, use rel='.'.
In comm and count, the output order is 'files then hashes'.
EOF`

# Check command line options and arguments.

if [ "$1" = "--help" ]; then usagf 0; fi

while getopts "h" option; do
  case "$option" in
    h) usagf 0 ;;
    *) exit 11 ;;
  esac
done

if [ "$#" -ne 3 ]; then
  echo "$0: there must be three non-option arguments" >&2
  exit 13
fi

checkdir "$s"
checkdir "$t"

# Functions.

check_sha1_for_dir() {
  xold="${2}_${3}.sha1"
  xcheck="${2}_${3}.check"

  # Using cd gets us relative paths to the hashed files.
  cd "$1/$2"
  shasum -a 1 -c "$xstore/$xold" > "$t/$xcheck" 2>&1
}

create_sha1_for_dir() {
  xhashf="${2}_${3}.sha1"
  if [ -e "$xstore/$xhashf" ]; then
    echo "$0: hash file already exists" >&2
    exit 31
  fi

  cd "$1/$2"
  find "$3" -type f | sort | xargs -r shasum -a 1 > "$xstore/$xhashf"
}

hash_count() {
  printf "%s\t%s\t%s\n" "$(find "$1/$2/$3" -type f | wc -l)" "files" "$2/$3"
  printf "%s\t%s\t%s\n" "$(wc -l "$s/flatf/hash/${2}_${3}.sha1" | cut -f1 -d' ')" "hashes" "$2/$3"
}

hash_comm() {
  tmp0=$(mktemp); tmp1=$(mktemp);
  [ -f "$tmp0" ] || exit 33
  [ -f "$tmp1" ] || exit 35
  (cd "$1/$2"; find "$3" -type f | sort > "$tmp0")
  (cut -f3 -d ' ' "$s/flatf/hash/${2}_${3}.sha1" | sed 's:\./::' | sort > "$tmp1")
  comm -3 "$tmp0" "$tmp1"
  rm -f "$tmp0" "$tmp1"
}

update_sha1_for_dir() {
  xold="${2}_${3}.sha1"
  xnew="${2}_${3}.new"
  if [ ! -e "$xstore/$xold" ]; then
    echo "$0: hash file does not exist" >&2
    exit 37
  fi

  cd "$1/$2"
  find "$3" -type f -newer "$xstore/$xold" | xargs -r shasum -a 1 > "$t/$xnew"
  cat "$t/$xnew" >> "$xstore/$xold"
  # A duplicate row is when two whole rows match, key and non-key.
  sort -uk 2 "$xstore/$xold" > "$t/zz_rothound_tmp"
  mv "$t/zz_rothound_tmp" "$xstore/$xold"
}

# Do the work.

xstore="$s/flatf/hash"
checkdir "$xstore"

# For example, if passed {'/etc', '.'}, we want {'/', 'etc', '.'}.
d2="$3"
d1=${2##*/}
d0=${2%"${d1}"}
checkdir "$d0/$d1/$d2"

case "$1" in
  check) check_sha1_for_dir "$d0" "$d1" "$d2" ;;
  comm) hash_comm "$d0" "$d1" "$d2" ;;
  count) hash_count "$d0" "$d1" "$d2" ;;
  create) create_sha1_for_dir "$d0" "$d1" "$d2" ;;
  update) update_sha1_for_dir "$d0" "$d1" "$d2" ;;
  *)
    echo "$0: valid subcommands are check, comm, count, create, update" >&2
    exit 21
    ;;
esac
