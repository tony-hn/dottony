BEGIN{FS="/"}
{if(NF > max) {line=$0; max=NF}}
END{if(length(line) > 0) print line; else print "."}
