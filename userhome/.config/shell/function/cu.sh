# Change up the directory tree by the given number of steps.
cu() {
  # Check arguments.
  if [ "$#" -gt 2 ]; then
    echo "cu(): Too many arguments" >&2
    return 1
  fi
  set -- "${1:-1}" "${2:-"$PWD"}"

  if [ "$1" -lt 0 ]; then
    echo "cu(): Invalid step count" >&2
    return 2
  fi

  # If the path is relative, anchor it.
  case "$2" in
    /*) ;;
    *) set -- "$1" "$PWD"/"$2" ;;
  esac

  while [ "$1" -gt 0 ]; do
    # Remove any trailing slashes.
    while :; do
      case "$2" in
        */) set -- "$1" "${2%/}" ;;
        *) break ;;
      esac
    done
    # Chop a path element.
    set -- "$(($1-1))" "${2%/*}"
  done

  [ -n "$2" ] && cd -- "$2" || return 3
}
