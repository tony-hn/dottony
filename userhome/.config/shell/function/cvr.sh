# Within a version control repo, change to the repo's root directory.
cvr() {
  xpath=""

  if git rev-parse --show-toplevel 2>&1 > /dev/null; then
    xpath=$(git rev-parse --show-toplevel)
  fi

  [ -n "$xpath" ] && cd -- "$xpath" || return 1
}
