# If it's a directory, change to it. If it's a file, change to its parent.
# Stands for "change smart".
cs() {
  if [ "$#" -ne 1 ]; then
    echo "cs(): Need one argument" >&2
    return 1
  fi

  # Remove any trailing slashes.
  # Anchor the path, if relative.
  while :; do
    case "$1" in
      */) set -- "${1%/}" ;;
      /*) break ;;
      *) set -- "$PWD"/"$1" ;;
    esac
  done

  # If the target isn't a directory, chop to its parent.
  if ! [ -d "$1" ]; then
    set -- "${1%/*}"
  fi

  [ -n "$1" ] && cd -- "$1" || return 2
}
