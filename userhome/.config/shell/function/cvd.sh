# Within a version control repo, take the unique list of the directories which
# contain a changed or added or deleted file, and change to the n^th one.
# If n is not specified, the default value is 1.
cvd() {
  set -- "${1:-1}"
  xpath=""

  if git status -s 2>&1 > /dev/null; then
    xpath=$(git status -s | sed 's/.. //' | xargs -r dirname |
      uniq | awk "FNR==${1}{print}")
  fi

  [ -n "$xpath" ] && cd -- "$xpath" || return 1
}
