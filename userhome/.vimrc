filetype plugin indent on
syntax on

colorscheme koehler
set backspace=indent,eol,start
set incsearch
set nobackup nowritebackup noswapfile
set noerrorbells
set nojoinspaces nowrap
set showmatch
set viminfo=""
set whichwrap+=<,>,[,]

if has("gui_running")
  set columns=80 lines=32
  set guioptions=ac
endif

" Tab handling by file type.
augroup filetypedetect
  autocmd! BufNewFile,BufRead *.[rRsS] setfiletype rlang
augroup END

set noexpandtab shiftwidth=8 tabstop=8
autocmd FileType sh setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType rlang setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType vim setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType c setlocal expandtab shiftwidth=4 tabstop=4
autocmd FileType cpp setlocal expandtab shiftwidth=4 tabstop=4

" Autofold org headers.
function OrgFold(lnum)
  let level = strlen(matchstr(getline(a:lnum), '\v^\s*\zs\*+'))
  if level > 0
    return '>'.level
  else
    return '='
  endif
endfunction

function! s:OrgSetting()
  setlocal foldmethod=expr foldexpr=OrgFold(v:lnum)
  setlocal foldtext=getline(v:foldstart).'...'.repeat('\ ',200)
endfunction

autocmd FileType org call s:OrgSetting()

