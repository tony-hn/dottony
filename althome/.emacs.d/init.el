; Mode-agnostic.
;; Functions for keys.
(defun z-backward-delete-sentence()
  (interactive)
  (delete-region (point) (save-excursion (backward-sentence 1) (point))))

(defun z-backward-delete-word()
  (interactive)
  (delete-region (point) (progn (backward-word 1) (point))))

(defun z-calc-no-buffer()
  (interactive)
  (require 'calc-ext)
  (calc-do-quick-calc)
  (kill-buffer "*Calculator*"))

(defun z-create-scratch-buffer()
  (interactive)
  (switch-to-buffer (get-buffer-create "*scratch*"))
  (lisp-interaction-mode))

(defun z-delete-sentence()
  (interactive)
  (delete-region (point) (save-excursion (forward-sentence 1) (point))))

(defun z-delete-word()
  (interactive)
  (delete-region (point) (progn (forward-word 1) (point))))

(defun z-kill-current-buffer-silently()
"Kill current buffer, without prompting the user for confirmation."
  (interactive)
  (not-modified)
  (kill-buffer))

(defun z-package-install()
"Some packages we use do not come with default emacs. Install them."
  (interactive)
  (require 'package)
  (add-to-list 'package-archives
    '("melpa" . "https://melpa.org/packages/"))
  (package-initialize)
  (package-refresh-contents)
  (unless (package-installed-p 'evil)
    (package-install 'evil)))

(defun z-query-replace-recenter()
"Query replace string then return to starting point and recenter."
  (interactive)
  (save-excursion (call-interactively 'query-replace))
  (recenter-top-bottom))

(defun z-query-replace-regexp-recenter()
"Query replace regexp then return to starting point and recenter."
  (interactive)
  (save-excursion (call-interactively 'query-replace-regexp))
  (recenter-top-bottom))

(defun z-unindent()
"Reduce the indentation level of the active region or line."
  (interactive)
  (let (xstart xend)
    (if (use-region-p)
      (setq xstart (region-beginning) xend (region-end))
      (setq xstart (line-beginning-position) xend (line-end-position)))
    (indent-rigidly xstart xend (- tab-width))))

(defun z-utf8-to-ascii-en()
"Replace some non-ASCII chars commonly found in English UTF-8 with ASCII chars."
  (interactive)
  (save-excursion ; Search for a hex code point.
    (goto-char (point-min)) (while (re-search-forward "\u20AC" nil t) (replace-match "EUR ")) ; Euro sign.
    (goto-char (point-min)) (while (re-search-forward "\u00A3" nil t) (replace-match "GBP ")) ; Pound sign.
    (goto-char (point-min)) (while (re-search-forward "\u00A5" nil t) (replace-match "JPY ")) ; Yen sign.
    (goto-char (point-min)) (while (re-search-forward "\u201C" nil t) (replace-match "\"")) ; Left double quotation mark.
    (goto-char (point-min)) (while (re-search-forward "\u201D" nil t) (replace-match "\"")) ; Right double quotation mark.
    (goto-char (point-min)) (while (re-search-forward "\u2013" nil t) (replace-match "-")) ; En dash.
    (goto-char (point-min)) (while (re-search-forward "\u2014" nil t) (replace-match "-")) ; Em dash.
    (goto-char (point-min)) (while (re-search-forward "\u2018" nil t) (replace-match "'")) ; Left single quotation mark.
    (goto-char (point-min)) (while (re-search-forward "\u2019" nil t) (replace-match "'")) ; Right single quotation mark.
    (goto-char (point-min)) (while (re-search-forward "\u2022" nil t) (replace-match "- ")) ; Bullet.
    (goto-char (point-min)) (while (re-search-forward "\u2026" nil t) (replace-match "...")) ; Horizontal ellipsis.
))

(defun z-win-path-to-mingw-path()
"Replace backslash with forward slash, delete colons, prepend forward slash,
within the current region."
  (interactive)
  (setq winpath (buffer-substring (region-beginning) (region-end)))
  (delete-region (region-beginning) (region-end))
  (insert (concat "/"
    (replace-regexp-in-string ":" "" 
      (replace-regexp-in-string (regexp-quote "\\") "/" winpath)))))

;; Key bindings.
(global-set-key (kbd "C-x DEL") 'z-backward-delete-sentence)
(global-set-key (kbd "M-DEL") 'z-backward-delete-word)
(global-set-key (kbd "C-=") 'z-calc-no-buffer)
(global-set-key (kbd "C-c s") 'z-create-scratch-buffer)
(global-set-key (kbd "M-k") 'z-delete-sentence)
(global-set-key (kbd "M-d") 'z-delete-word)
(global-set-key (kbd "C-x k") 'z-kill-current-buffer-silently)
(global-set-key (kbd "M-\%") 'z-query-replace-recenter)
(global-set-key (kbd "M-\"") 'z-query-replace-regexp-recenter)
(global-set-key (kbd "S-<tab>") 'z-unindent)
(global-set-key (kbd "<backtab>") 'z-unindent)
(global-set-key (kbd "C-c u") 'z-utf8-to-ascii-en)
(global-set-key (kbd "C-c m") 'z-win-path-to-mingw-path)

(global-set-key (kbd "M-p") 'mark-word)

(put 'downcase-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'upcase-region 'disabled nil)

;; Editing defaults.
(delete-selection-mode t)
(setq sentence-end-double-space nil)
(setq-default major-mode 'text-mode)
(show-paren-mode t)

; Line wrap.
(setq-default fill-column 80) ; Break lines at 80 characters.
(setq-default truncate-lines t) ; Turn off visual-line-mode by default.

;; Concise prompts.
(defalias 'yes-or-no-p 'y-or-n-p)

;; Scrolling.
(setq scroll-conservatively 1000)
(setq recenter-positions '(top middle bottom))

;; No Completions, Messages buffer. No scratch if opened with another buffer.
(setq completion-auto-help nil)
(defun z-emacs-startup-hook()
  (if (get-buffer "*Messages*") (kill-buffer "*Messages*"))
  (if (get-buffer "*scratch*") (kill-buffer "*scratch*")))
(add-hook 'emacs-startup-hook 'z-emacs-startup-hook)

;; Buffer decluttering.
(setq inhibit-startup-message t)
(setq inhibit-startup-screen t)
(setq initial-scratch-message nil)
(setq message-log-max nil)

;; Frame decluttering.
(set-fringe-mode 0)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)

;; Remove audio-visual distractions.
(blink-cursor-mode 0)
(setq visible-bell 1)
(setq echo-keystrokes 0.1)

;; Stop creating junk files.
(setq auto-save-default nil)
(setq auto-save-list-file-name nil)
(setq make-backup-files nil)

;; Uniquify buffer names.
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Colour scheme.
;;; If we want to change colour scheme via the GUI, comment this out first.
;;; monochrome = 2e2*f40
(custom-set-variables
 '(custom-enabled-themes (quote (monochrome)))
 '(custom-safe-themes (quote ("2e24f6cb463262b950cb3a4e471bbacce3c48c57089750ccf651e8e17f327f40" default))))
(set-cursor-color "#f5f5f5")

;; Font.
(defvar z-preferred-font
  '("Inconsolata Regular", "Consolas",
    "Bitstream Vera Sans Mono Roman", "BitstreamVeraSansMono",
    "DejaVu Sans Mono Book", "DejaVuSansMono",
    "PT Mono"))

(defun z-set-preferred-font(&optional frame)
"Set the first available font from `z-preferred-font'."
  (catch 'done
    (with-selected-frame (or frame (selected-frame))
      (dolist (font z-preferred-font)
        (when (ignore-errors (x-list-fonts font))
          (set-frame-font font)
          (throw 'done nil))))))

;;; If we want to change font via the GUI, comment this out first.
(z-set-preferred-font)
;(add-hook 'after-make-frame-functions #'z-set-preferred-font)

; Mode-specific.
;; CC mode.
(setq c-default-style "linux")
(setq-default c-basic-offset 4)
(setq-default c-syntactic-indentation nil)

;; Evil mode.
(evil-mode 1)
(setq x-select-enable-clipboard nil)

;; sh-mode.
(setq-default sh-basic-offset 2)
(defun z-sh-mode-hook()
  (electric-indent-local-mode -1))
(add-hook 'sh-mode-hook 'z-sh-mode-hook)

;;; Key bindings.
(defun z-cedet-hook()
  (local-set-key "\C-cj" 'semantic-ia-fast-jump) ; Replace semantic-complete-jump-local.
  (local-set-key "\C-cs" 'semantic-ia-show-summary) ; Don't think this replaces anything.
  (local-set-key "\C-cc" 'semantic-ia-complete-tip) ; Replace semantic-analyze-possible-completions.
  (c-toggle-electric-state -1)
  (semantic-mode 1)
  (setq indent-tabs-mode nil)
  (setq tab-width c-basic-offset))
(add-hook 'c-mode-common-hook 'z-cedet-hook)

;; Dired.
;;; Show less information.
(require 'ls-lisp)
(setq ls-lisp-use-insert-directory-program nil)
(setq ls-lisp-verbosity nil)

;; LaTeX mode.
(defun z-tex-mode-hook()
  (turn-on-auto-fill))
(add-hook 'tex-mode-hook 'z-tex-mode-hook)
;(fset 'tex-font-lock-subscript 'ignore) ; No underscore as subscript. BROKEN.

;; Org mode.
(require 'org-tempo nil t)
(setq org-startup-folded t)
(defun z-org-mode-hook()
  (turn-on-auto-fill)
  (electric-indent-local-mode -1)
  (setq org-link-frame-setup (quote (
    (vm . vm-visit-folder-other-frame)
    (vm-imap . vm-visit-imap-folder-other-frame)
    (gnus . org-gnus-no-new-news)
    (file . find-file) ; Changed from find-file-other-window.
    (wl . wl-other-frame))))
  (setq org-mouse-features (quote (
    yank-link activate-stars activate-bullets activate-checkboxes))))
(add-hook 'org-mode-hook 'z-org-mode-hook)

;; SQL mode.
(add-hook 'sql-mode-hook 'z-cedet-hook)

;; Text mode.
(defun z-text-mode-hook()
  (setq indent-tabs-mode t)
  (setq-default tab-width 8))
(add-hook 'text-mode-hook 'z-text-mode-hook)
